'use strict'

let gulp = require('gulp');
let sass = require('gulp-sass');
let connect = require('gulp-connect');

gulp.task('connect', () => 
    connect.server({livereload: true}));

gulp.task('sass', () => 
    gulp.src('./style/scss/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./style/css')));

gulp.task('watch', () => {
    gulp.watch('./style/scss/**/*.scss', ['sass'])
});


gulp.task('default', ['connect', 'watch']);
