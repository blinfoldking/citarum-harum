app.controller('appKema', ($scope, $rootScope, $http) => {
    $scope.state = 'HOME'
    $scope.maxjob = 1
    $scope.getJobs = () => {
        $http.get('https://api.citarumharum.id/api/allJobs', {
                headers: {
                    Authorization: 'Bearer ' + $rootScope.kema_token
                }
            }).then(res => $scope.jobs = res.data)
            .then(() => console.log($scope.jobs.data))
    }

    $scope.getStudent = () => {
        console.log($scope.currData)
        $http.get('https://api.citarumharum.id/api/student/institution/' + $scope.currData["ID"], {
                headers: {
                    Authorization: 'Bearer ' + $rootScope.kema_token
                }
            }).then(res => $scope.student = res.data)
            .then(() => console.log($scope.student))
            .then(() => $scope.state = 'MHS_LIST_TABLE')
    }

    $scope.getInstitution = () => {
        $http.get('https://api.citarumharum.id/api/allInstitution', {
                headers: {
                    Authorization: 'Bearer ' + $rootScope.kema_token
                }
            }).then(res => $scope.Institution = res.data.data)
            .then(() => console.log($scope.Institution))
    }

    $scope.getStudentDetail = (id) => {
        $http.get('https://api.citarumharum.id/api/student/id/' + $scope.student.data[id]["ID"], {
                headers: {
                    Authorization: 'Bearer ' + $rootScope.kema_token
                }
            }).then(res => $scope.studentDetail = res.data.data)
            .then(() => console.log($scope.studentDetail))
    }
    
    $scope.selectJob = (id) => {
        $scope.selectedJob = id;
        $scope.state = 'MHS_INPUT';
    }

    $scope.postStudent = (name, nim, prodi, phone, email, sumSks, currSks) => {
        $http.post('https://api.citarumharum.id/api/postStudent/', {
                headers: {
                    Authorization: 'Bearer ' + $rootScope.kema_token
                },
                body: {
                    name: name,
                    student_id: nim,
                    major: prodi,
                    phone_number: phone,
                    email: email,
                    credit_passed: sumSks,
                    credit_ongoing: currSks,
                    job: $scope.selectedJob
                }
            }).then(res => $scope.studentDetail = res.data.data)
            .then(() => console.log($scope.studentDetail))
            .catch(err => console.log(err));
    }

    $scope.getInstitution()
    $scope.getJobs()
    $scope.selectedJob = 0;
    $scope.selectedInstitution = 0;
    
    (function () {
        $http.get('https://api.citarumharum.id/api/me/data', {
            headers: {
                Authorization: 'Bearer ' + $rootScope.kema_token
            }
        }).then(res => $scope.currData = res.data.data)
        .then(() => console.log($scope.currData))
    }())
    // $scope.getStudent()
    // console.log($scope.currData)
})
