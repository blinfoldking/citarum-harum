app.controller('appBelmawa', ($scope, $rootScope, $http) => {
    $scope.state = 'HOME'
    $scope.maxjob = 1
    $scope.getJobs = () => {
        $http.get('https://televicient.tk/api/allJobs', {
                headers: {
                    Authorization: 'Bearer ' + $rootScope.belmawa_token
                }
            }).then(res => $scope.jobs = res.data)
            .then(() => console.log($scope.jobs.data))
    }

    $scope.getStudent = () => {
        console.log($scope.currData)
        $http.get('https://televicient.tk/api/student/institution/' + $scope.currData["ID"], {
                headers: {
                    Authorization: 'Bearer ' + $rootScope.belmawa_token
                }
            }).then(res => $scope.student = res.data)
            .then(() => console.log($scope.student))
            .then(() => $scope.state = 'MHS_LIST_TABLE')
    }

    $scope.getInstitution = () => {
        $http.get('https://televicient.tk/api/allInstitution', {
                headers: {
                    Authorization: 'Bearer ' + $rootScope.belmawa_token
                }
            }).then(res => $scope.Institution = res.data.data)
            .then(() => console.log($scope.Institution))
    }

    $scope.getStudentDetail = (id) => {
        $http.get('https://televicient.tk/api/student/id/' + $scope.student.data[id]["ID"], {
                headers: {
                    Authorization: 'Bearer ' + $rootScope.belmawa_token
                }
            }).then(res => $scope.studentDetail = res.data.data)
            .then(() => console.log($scope.studentDetail))
    }

    $scope.getInstitution()
    $scope.getJobs()
    $scope.selectedJob = 0;
    $scope.selectedInstitution = 0;

    (function () {
        $http.get('https://televicient.tk/api/me/data', {
                headers: {
                    Authorization: 'Bearer ' + $rootScope.belmawa_token
                }
            }).then(res => $scope.currData = res.data.data)
            .then(() => console.log($scope.currData))
    }())
    // $scope.getStudent()
    // console.log($scope.currData)
})