var app = angular.module('citarumHarum', ['ngRoute'])

app.config(($routeProvider) => {
    $routeProvider
        .when('/app/belmawa/login', {
            templateUrl: './view/app-belmawa-login.html'
        })
        .when('/app/belmawa/home', {
            templateUrl: './view/app-belmawa.html'
        })
        .when('/app/kema/login', {
            templateUrl: './view/app-kema-login.html'
        })
        .when('/app/kema/home', {
            resolve: {
                "check": ($rootScope, $location) => {
                    if (!$rootScope.kema_isLogin) 
                        $location.path('/view/app-kema-login')                        
                }
            },
            templateUrl: './view/app-kema.html' 
        })
        .when('/app/eval/login', {
            templateUrl: './view/app-eval-login.html'
        })
        .when('/app/eval/home', {
            templateUrl: './view/app-eval.html'
        })
        .otherwise({
            redirectTo: '/app/kema/login'
        })
})
